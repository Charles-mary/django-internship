from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class ScrumyGoals(models.Model):
    goal_id = models.IntegerField()
    goal_name = models.CharField(max_length=255)
    created_by = models.CharField(max_length=255, blank=True, null=True)
    moved_by = models.CharField(max_length=255, blank=True, null=True)
    owner = models.CharField(max_length=255, blank=True, null=True)
    user = models.ForeignKey(User, related_name='user_goals', on_delete=models.CASCADE)
    goal_status = models.ForeignKey("GoalStatus", related_name="goals", on_delete=models.PROTECT)

    def __str__(self):
        return self.goal_name

class ScrumyHistory(models.Model):
    moved_by = models.CharField(max_length=255)
    created_by = models.CharField(max_length=255)
    moved_from = models.CharField(max_length=255)
    moved_to = models.CharField(max_length=255)
    time_of_action = models.DateTimeField()
    goal = models.ForeignKey("ScrumyGoals", on_delete=models.PROTECT)

    def __str__(self):
        return self.created_by
    
class GoalStatus(models.Model):
    status_name = models.CharField(max_length=255)

    def __str__(self):
        return self.status_name


