from django.contrib.auth.models import Group
from django.contrib.auth.hashers import make_password
from django.contrib.auth import logout
from django.contrib import messages
from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import ScrumyGoals, GoalStatus, User
from .forms import CreateGoalForm, SignUpForm, MoveGoalForm
import random
# Create your views here.


def index(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            new_user = form.save(commit=False)
            new_user.password = make_password(form.cleaned_data['password'])
            new_user.save()

            developer_group = Group.objects.get(name="Developer")
            new_user = User.objects.get(username=form.cleaned_data['username'])
            developer_group.user_set.add(new_user)
            messages.success(request, 'Your account has been created sucessfully')
            return redirect('/charlesmaryayoscrumy/accounts/login', permanent=True) 
    else:
        form = SignUpForm()
    return render(request, 'index.html', {'form' : form})


def move_goal(request, goal_id):

    current_user = request.user

    dictionary = {
        'error': "There is no record with that goal id"
    }

    try:
        goal = ScrumyGoals.objects.get(goal_id=goal_id)
    except Exception as e:
        return render(request, 'exception.html', dictionary)
    
    if request.method == 'POST':
        form = MoveGoalForm(request.POST)
        if form.is_valid():

            if current_user.groups.filter(name="Developer").exists():
                if form.cleaned_data['status_name'] == "Done Goal":
                    messages.error(request, "Special Permissions are required to perform this operation.")
                    return redirect('charlesmaryayoscrumy:home', permanent=True)
                if goal.user != current_user:
                    messages.error(request, """Special Permissions are required to perform this operation. 
                    Try moving a goal you've created""")
                    return redirect('charlesmaryayoscrumy:home', permanent=True)

            if current_user.groups.filter(name="Quality Assurance").exists():
                if form.cleaned_data['status_name'] == "Weekly Goal":
                    messages.error(request, "Special Permissions are required to perform this operation")
                    return redirect('charlesmaryayoscrumy:home', permanent=True)
                if goal.user != current_user and goal.goal_status.status_name != "Verify Goal":
                    messages.error(request, "You can only move goal's of others if they have been verified")
                    return redirect('charlesmaryayoscrumy:home', permanent=True)

            if current_user.groups.filter(name="Owner").exists():
                if goal.user != current_user:
                    messages.error(request, """Special Permissions are required to perform this operation.
                    Try moving a goal you've created""")
                    return redirect('charlesmaryayoscrumy:home', permanent=True)

            goal.goal_status = GoalStatus.objects.get(status_name=form.cleaned_data['status_name'])
            goal.save()
            messages.success(request, f"The goal has been sucessfully moved to {form.cleaned_data['status_name']}")
            return redirect('charlesmaryayoscrumy:home', permanent=True)
    else:
        form = MoveGoalForm()
    return render(request, 'movegoal.html', {'form':form})


def add_goal(request):

    current_user = request.user
    if request.method == "POST":
        form = CreateGoalForm(request.POST)
        if form.is_valid():
            form.save(commit=False)
            if current_user.groups.first().name in ["Developer", "Quality Assurance", "Owner"]:
                if form.cleaned_data['user'] != current_user or \
                    form.cleaned_data['goal_status'].status_name != "Weekly Goal":
                    messages.error(request, "You can only create a weekly goal for yourself")
                    return render(request, 'addgoal.html', {'form': form})
            form.save()
            messages.success(request, "Goal created successfully")
            return redirect('charlesmaryayoscrumy:home', permanent=True)
    else:
        form = CreateGoalForm()

    return render(request, 'addgoal.html', {'form': form})


def home(request):
    data = {
        'users': User.objects.all(),
        'weekly': GoalStatus.objects.get(status_name="Weekly Goal").goals.all(),
        'daily': GoalStatus.objects.get(status_name="Daily Goal").goals.all(),
        'verify': GoalStatus.objects.get(status_name="Verify Goal").goals.all(),
        'done': GoalStatus.objects.get(status_name="Done Goal").goals.all()
    }
    return render(request, 'home.html', data)


def logout_view(request):
    logout(request)
    redirect('charlesmaryayoscrumy:login', permanent=True)